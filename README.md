## Analysis of abnormal data

The theme of this dataset is that it's not normally distributed.  It's also pretty messy in the original format, and so requires some cleaning before we can do anything with it without too much trouble.

### Questions of interest

This is data from a category learning task with a two-by-three design.  There are two types of categorization tasks (II and RB), and three different response/feedback manipulations (AB, YN, and YNX).

The theoretically interesting questions are: is performance on the two tasks equivalent in some conditions but not others? In particular, is performance on the RB and II tasks equal in the "baseline" AB condition? The YN condition is supposed to be "hard" for the II task but not the RB task, so is this what happens? The YNX condition is supposed to be less hard for the II task, so how does this relate to the other two conditions?

### Structure of the dataset

The condition labels, as well as the unique subject number, are stored in columns in the data file.  This particular data file aggregates responses from each subject into average accuracy for each of four blocks of 80 trials, which are stored in columns.  There's some other junk from whatever program spit out this file (SPSS, maybe?) but that's easy to remove.

`analysis.R` has my data cleaning code, which uses `reshape` to convert this "wide" format (one column per observation/block) to a "long" format (each observation on its own row, with block as another variable).  This makes it easier to put into `lm` etc.  It also has code to generate some visualizations of the data.

## Analyses to do

1. Compute 90% quantiles in each condition, with boostrapped confidence intervals.  Hint: use `quantile` and `sample` as in the bootstrapping example.
2. Permutation test between conditions or blocks.  For instance, compare the mean accuracy in block 4 for two conditions, and use a permutation test to get the null hypothesis distribution of the difference in means.
3. Simulation for bimodality.  Dream up a test statistic that would distinguish between a bimodal distribution (like a mixture of two gaussians with different means), and write a function to compute it (for inspiration, see the example).  Then apply it to simulated data to see how well it works.  For instance, you could simulate many data sets, each samples from two gaussians with but varying differences between the means to see how much of a difference there has to be to reliably detect bimodality.   
